from collections import deque
from functools import reduce
from math import floor

def parse_input(inputfile='input.in'):
    with open(inputfile) as fp:
        return list(map(lambda l: l.strip(), fp.readlines()))


def part1(lines):
    chunkChars = {'<':'>','(':')','[':']','{':'}'}
    chunkScore = {')': 3, ']':57, '}':1197, '>':25137}
    score = 0
    for l in lines:
        stack = deque()
        for c in l:
            if c in chunkChars.keys():
                stack.append(c)
            elif c == chunkChars[stack[-1]]:
                stack.pop()
            else:
                score += chunkScore[c]
                stack.pop()
    return score


def part2(lines):
    chunkChars = {'<':'>','(':')','[':']','{':'}'}
    chunkScore = {')': 1, ']':2, '}':3,'>':4}
    scores = []
    for l in lines:
        stack = deque()
        corrupted = False
        for c in l:
            if c in chunkChars.keys():
                stack.append(c)
            elif c == chunkChars[stack[-1]]:
                stack.pop()
            else:
                corrupted = True
        if not corrupted:
            scores.append(
                reduce(lambda acum, c:
                    (acum * 5) + chunkScore[chunkChars[c]], reversed(stack), 0
                )
            )            

    return sorted(scores)[floor(len(scores)/2)]


if __name__ == "__main__":
    parsed = parse_input()
    sol1 = part1(parsed)
    print('Part 1 solution:', sol1)
    sol2 = part2(parsed)
    print('Part 2 solution:', sol2)