import numpy as np

def load_input(inputfile='input.txt'):
     with open(inputfile) as fp: 
        return fp.readlines()


def parse_input(lines):
    return list(map(lambda l: list(l.strip()), lines))


def part1(lines):
    length = len(lines)
    gamma = epsilon = ''

    for bl in np.array(lines).T.tolist():
        mostcommon = len(list(filter(lambda l: l == '1', bl))) > length/2
        gamma += '1' if mostcommon else '0'
        epsilon += '0' if mostcommon else '1'

    return int(gamma, 2) * int(epsilon, 2)
        

def part2(lines):
    ox = lines.copy()
    cx = lines.copy()

    def strip(bl,idx,ff):
        if len(bl) == 1:
            return bl
        
        nbl = {'1': [], '0': []}
        for bit in bl:
            nbl[bit[idx]].append(bit)
        
        return ff(nbl['0'], nbl['1'])
    
    for i in range(len(lines[0])):
        ox = strip(ox, i, lambda z,o: z if len(z) > len(o) else o)
        cx = strip(cx, i, lambda z,o: o if len(z) > len(o) else z)

    return int(''.join(ox[0]), 2) * int(''.join(cx[0]), 2)


if __name__ == "__main__":
    input = load_input()
    parsed = parse_input(input)

    sol1 = part1(parsed)
    print('Part 1 solution:', sol1)
    sol2 = part2(parsed)
    print('Part 2 solution:', sol2)