
def parse_input(inputfile='input.in'):
    with open(inputfile) as fp:
        return list(map(lambda l: l.strip(), fp.readlines()))


def part1(lines):
    return 0


def part2(lines):
    return 0


if __name__ == "__main__":
    parsed = parse_input()
    sol1 = part1(parsed)
    print('Part 1 solution:', sol1)
    sol2 = part2(parsed)
    print('Part 2 solution:', sol2)