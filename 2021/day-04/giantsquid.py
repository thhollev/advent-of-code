def load_input(inputfile='input.txt'):
     with open(inputfile) as fp: 
        return fp.readlines()


def parse_input(lines):
    stripped = list(map(lambda l: l.strip(), lines))
    numbers = stripped[0].split(',')
    boards = []
    scores = []
    buffer = []
    for b in list(map(lambda l: l.split(), stripped[2:])):
        if not b:
            boards.append(buffer)
            buffer = []
            scores.append([[False for _ in range(5)] for _ in range(5)])
        else:
            buffer.append(list(map(lambda l: int(l), b)))
    return [numbers, boards, scores]


def mark_score(number, board, score):
    for i in range(5):
        for j in range(5):
            if board[i][j] == number:
                score[i][j] = True
                return


def get_score(board, score):
    count = 0
    for j in range(5):
        for k in range(5):
            if not score[j][k]:
                count += board[j][k]
    return count


def eval_score(number, board, score):
    won = False

    for i in range(5):
        full_row = True
        for j in range(5):
            if not score[i][j]:
                full_row = False
        if full_row:
            won = True

    for j in range(5):
        full_col = True
        for i in range(5):
            if not score[i][j]:
                full_col = False
        if full_col:
            won = True

    if won:
       return get_score(board, score)

    return -1        
    
def part1(numbers, boards, scores):
    for number in numbers:
        for i in range(len(boards)):
            mark_score(int(number), boards[i], scores[i])
            s = eval_score(int(number), boards[i], scores[i])
            if s != -1:
                return s * int(number)
        

def part2(numbers, boards, scores):
    last_board = None
    last_score = None

    for number in numbers:
        filtered_boards = []
        filtered_scores = []
        for i,board in enumerate(boards):
            mark_score(int(number), board, scores[i])
            s = eval_score(int(number), board, scores[i])
            if s == -1:
                filtered_boards.append(board)
                filtered_scores.append(scores[i])
        boards = filtered_boards
        scores = filtered_scores
        if len(boards) == 1:
            last_board = boards[0]
            last_score = scores[0]
        if len(boards) == 0:
            return get_score(last_board, last_score) * int(number)

if __name__ == "__main__":
    input = load_input()
    [numbers, boards, scores] = parse_input(input)

    sol1 = part1(numbers, boards, scores)
    print('Part 1 solution:', sol1)
    sol2 = part2(numbers, boards, scores)
    print('Part 2 solution:', sol2)