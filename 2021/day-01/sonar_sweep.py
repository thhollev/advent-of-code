
def load_input(inputfile='input.txt'):
     with open(inputfile) as fp: 
        return fp.readlines()


def parse_input(lines):
    return list(map(lambda l: (int)(l.strip()), lines))


def part1(numbers):
    return sum([numbers[i] > numbers[i-1] for i in range(1, len(numbers))])


def part2(numbers):
    return sum([numbers[i] > numbers[i-3] for i in range(3, len(numbers))])


if __name__ == "__main__":
    input = load_input()
    parsed = parse_input(input)

    sol1 = part1(parsed)
    print('Part 1 solution:', sol1)
    sol2 = part2(parsed)
    print('Part 2 solution:', sol2)] for i in range(1, len(numbers))])
