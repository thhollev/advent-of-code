from operator import mul
from functools import reduce


def parse_input(inputfile='input.in'):
    with open(inputfile) as fp:
        lines = list(map(lambda l: l.strip(), fp.readlines()))
        return list(map(list,lines))


def isLowest(area, x, y):
    point = int(area[y][x])
    adjacent = []
    
    # Left
    if x > 0:
        adjacent.append(int(area[y][x-1]))
    # Right
    if x < len(area[y])-1:
        adjacent.append(int(area[y][x+1]))
    # Up
    if y > 0:
        adjacent.append(int(area[y-1][x]))
    # Down
    if y < len(area)-1:
        adjacent.append(int(area[y+1][x]))
    
    return all([a > point for a in adjacent])


def part1(area):
    risk_level = 1
    lowest_points = []
    for i in range(len(area)):
        for j in range(len(area[i])):
            if isLowest(area, j, i):
                lowest_points.append(int(area[i][j]) + risk_level)
    return sum(lowest_points)


def explore_basin(area, x, y, basin=set()):
    point = int(area[y][x])
    
    if point == 9:
        return basin
    else:
        # Mark point as viewed
        area[y][x] = 9
        basin.add((x,y))
    
    # Left
    if x > 0:
        explore_basin(area, x-1, y, basin)
    # Right
    if x < len(area[y])-1:
        explore_basin(area, x+1, y, basin)
    # Up
    if y > 0:
        explore_basin(area, x, y-1, basin)
    # Down
    if y < len(area)-1:
        explore_basin(area, x, y+1, basin)

    return basin


def part2(area):
    basins = []
    for i in range(len(area)):
        for j in range(len(area[i])):
            if area[i][j] != 9:
                basins.append(len(explore_basin(area, j, i, set())))

    return reduce(mul, (sorted(basins)[-3:]), 1)


if __name__ == "__main__":
    parsed = parse_input()
    sol1 = part1(parsed)
    print('Part 1 solution:', sol1)
    sol2 = part2(parsed)
    print('Part 2 solution:', sol2)