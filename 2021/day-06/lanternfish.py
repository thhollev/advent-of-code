import math

def load_input(inputfile='input.txt'):
    with open(inputfile) as fp: 
        return fp.read()


def parse_input(lines):
    return list(map(lambda l: int(l),lines.split(',')))


def part1(fish, days=80):
    for _ in range(days):
        fish_one_day_later = []
        fish_new = []
        for f in fish:
            f -= 1
            if f == -1:
                f = 6
                fish_new.append(8)
            fish_one_day_later.append(f)
        fish_one_day_later.extend(fish_new)
        fish = fish_one_day_later
    
    return len(fish)


def part2(lines):
    fish = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0}

    for num in lines:
        fish[num] += 1

    for _ in range(256):
        amount_birth = fish[0]
        for i in range(8):
            fish[i] = fish[i+1]
        fish[8] = amount_birth
        fish[6] += amount_birth
        
    return (sum(fish.values()))


if __name__ == "__main__":
    input = load_input()
    parsed = parse_input(input)

    sol1 = part1(parsed)
    print('Part 1 solution:', sol1)
    sol2 = part2(parsed)
    print('Part 2 solution:', sol2)