from itertools import chain

def load_input(inputfile='input.txt'):
    with open(inputfile) as fp: 
        return fp.readlines()


def parse_input(lines):
    return list(map(lambda l: list(map(lambda k: k.split(',') ,l.strip().split(' -> '))), lines))


def part1(lines, diagonal=False):
    vents =  [[0 for _ in range(1000)] for _ in range(1000)]

    for l in lines:
        x1 = int(l[0][0])
        x2 = int(l[1][0])
        y1 = int(l[0][1])
        y2 = int(l[1][1])
        
        if x1 == x2:
            if y1 < y2:
                for i in range(y1,y2+1):
                    vents[i][x1] += 1
            else:
                for i in range(y1,y2-1,-1):
                    vents[i][x1] += 1
        elif y1 == y2:
            if x1 < x2:
                for i in range(x1,x2+1):
                    vents[y1][i] += 1
            else:
                for i in range(x1,x2-1,-1):
                    vents[y1][i] += 1
        elif diagonal:
            if x1 < x2 and y1 < y2:
                while x1 <= x2 or y1 <= y2:
                    vents[y1][x1] += 1
                    x1 += 1
                    y1 += 1
            elif x1 < x2 and y1 > y2:
                while x1 <= x2 or y1 >= y2:
                    vents[y1][x1] += 1
                    x1 += 1
                    y1 -= 1
            elif x1 > x2 and y1 > y2:
                while x1 >= x2 or y1 >= y2:
                    vents[y1][x1] += 1
                    x1 -= 1
                    y1 -= 1
            elif x1 > x2 and y1 < y2:
                while x1 >= x2 or y1 <= y2:
                    vents[y1][x1] += 1
                    x1 -= 1
                    y1 += 1
    
    return len(list(filter(lambda l: l >= 2, chain(*vents))))


def part2(lines):
    return part1(lines, diagonal=True)


if __name__ == "__main__":
    input = load_input()
    parsed = parse_input(input)

    sol1 = part1(parsed)
    print('Part 1 solution:', sol1)
    sol2 = part2(parsed)
    print('Part 2 solution:', sol2)