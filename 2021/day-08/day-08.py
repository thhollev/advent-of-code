from collections import Counter
from functools import reduce


def parse_input(inputfile='input.in'):
    with open(inputfile) as fp:
        lines = list(map(lambda l: l.strip().split(' | '), fp.readlines()))
        return list(map(lambda l: (l[0].split(), l[1].split()), lines))

segments_to_number = {
    "abcefg": 0,
    "cf": 1,
    "acdeg": 2,
    "acdfg": 3,
    "bcdf": 4,
    "abdfg": 5,
    "abdefg": 6,
    "acf": 7,
    "abcdefg": 8,
    "abcdfg": 9,
}

def part1(lines):
    appear = 0
    for line in lines:
        for s in line[1]:
            if len(s) in [2,3,4,7]:
                appear += 1

    return appear


def part2(lines):
    sum_of_outputs = 0

    for pattern,output in lines:

        input = "".join(pattern)
        char_count = Counter(reduce(lambda x, y: x + y,input))
        decoded = {}

        seven = next(x for x in pattern if len(x) == 3)
        one = next(x for x in pattern if len(x) == 2)
        a_char = next(a for a in seven if a not in one)
        decoded[a_char] = 'a'

        for char, count in char_count.items():
            if count == 4:
                decoded[char] = "e"
            elif count == 6:
                decoded[char] = "b"
            elif count == 9:
                decoded[char] = "f"
            elif count == 8 and char != a_char:
                decoded[char] = "c"
            elif count == 7:
                four = next(x for x in pattern if len(x) == 4)
                if char in four:
                    decoded[char] = "d"
                else:
                    decoded[char] = "g"

        number = ''
        for p in output:
            np = "".join(sorted(list(map(lambda l: decoded[l],p))))
            number += str(segments_to_number[np])
        sum_of_outputs += int(number)

    return sum_of_outputs


if __name__ == "__main__":
    parsed = parse_input()
    sol1 = part1(parsed)
    print('Part 1 solution:', sol1)
    sol2 = part2(parsed)
    print('Part 2 solution:', sol2)