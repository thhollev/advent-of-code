
def load_input(inputfile='input.txt'):
     with open(inputfile) as fp: 
        return fp.readlines()


def parse_input(lines):
    return list(map(lambda l: (l.strip()).split(" "), lines))


def part1(lines):
    moves = {'up': 0, 'down': 0, 'forward': 0}
    for [move, num] in lines:
        moves[move] += int(num)
    return moves['forward'] * (moves['down'] - moves['up'])


def part2(lines):
    moves = {'up': 0, 'down': 0, 'forward': 0, 'depth': 0}
    for [move, num] in lines:
        moves[move] += int(num)
        if move == 'forward':
            moves['depth'] += (moves['down'] - moves['up']) * int(num)

    return  moves['forward'] * moves['depth']


if __name__ == "__main__":
    input = load_input()
    parsed = parse_input(input)

    sol1 = part1(parsed)
    print('Part 1 solution:', sol1)
    sol2 = part2(parsed)
    print('Part 2 solution:', sol2)