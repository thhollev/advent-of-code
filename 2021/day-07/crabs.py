import math, sys, time

def load_input(inputfile='input.txt'):
    with open(inputfile) as fp: 
        return fp.read()


def parse_input(lines):
    return list(map(lambda l: int(l),lines.split(',')))


def part1(crabs):
    crabs = sorted(crabs)
    r = range(int(len(crabs)/2))

    return min([
        sum([abs(crab - i) for crab in crabs]) for i in r
    ])

def part2(crabs):
    crabs = sorted(crabs)
    r = range(int(len(crabs)/2))

    return min([
        sum([sum(range((abs(crab - i)) + 1)) for crab in crabs]) for i in r
    ])


if __name__ == "__main__":
    input = load_input()
    parsed = parse_input(input)

    sol1 = part1(parsed)
    print('Part 1 solution:', sol1)
    sol2 = part2(parsed)
    print('Part 2 solution:', sol2)