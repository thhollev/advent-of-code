<?php

function parse_file() : array
{
    $contents =  preg_split("/\n/", rtrim(file_get_contents('./input.in')));
    return array_map(function($n) { return (int)$n; }, $contents);
}

function part1(array $lines): int
{
    foreach($lines as $l1) {
        foreach($lines as $l2) {
            if($l1 + $l2 == 2020) {
                return $l1 * $l2;
            }
        }
    }

    return 0;
}

function part2(array $lines): int
{
    foreach($lines as $l1) {
        foreach($lines as $l2) {
            foreach($lines as $l3) {
                if ($l1 + $l2 + $l3 == 2020) {
                    return ($l1 * $l2 * $l3);
                }
            }
        }
    }

    return 0;
}

function solve(): void
{
    $parsed = parse_file();

    $sol1 = part1($parsed);
    echo 'Solution part1 ' . $sol1 . PHP_EOL;
    $sol2 = part2($parsed);
    echo 'Solution part2 ' . $sol2 . PHP_EOL;

}

solve();