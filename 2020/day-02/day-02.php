<?php

function parse_file(): array
{
    $contents = preg_split("/\n/", rtrim(file_get_contents('./input.in')));
    return array_map(function ($n) {
        $v = preg_split("/ /", $n);
        $mm = preg_split("/-/", $v[0]);
        return [$mm[0], $mm[1], str_replace(':', '', $v[1]), $v[2]];
    }, $contents);
}

function part1(array $passwords): int
{
    $valid = 0;

    foreach ($passwords as $pw) {
        $count = substr_count($pw[3],$pw[2]);
        if ($pw[0] <= $count and $count <= $pw[1]) {
            $valid++;
        }
    }

    return $valid;
}

function part2(array $passwords): int
{
    $valid = 0;

    foreach ($passwords as $pw) {
        $count = substr_count($pw[3][(int)$pw[0]-1],$pw[2]) + substr_count($pw[3][(int)$pw[1]-1],$pw[2]);
        if ($count == 1)  {
            $valid++;
        }
    }

    return $valid;
}

function solve(): void
{
    $parsed = parse_file();

    $sol1 = part1($parsed);
    echo 'Solution part1 ' . $sol1 . PHP_EOL;
    $sol2 = part2($parsed);
    echo 'Solution part2 ' . $sol2 . PHP_EOL;

}

solve();