<?php

function parse_file(): array
{
    $contents = preg_split("/\n/", file_get_contents('./input.in'), -1, PREG_SPLIT_NO_EMPTY);
    return array_map(function ($l) {
        return preg_split("//", $l, -1, PREG_SPLIT_NO_EMPTY);
    }, $contents);
}

function part1(array $lines): int
{
    return 0;
}

function part2(array $lines): int
{
    return 0;
}

function solve(): void
{
    $parsed = parse_file();
    $sol1 = part1($parsed);
    echo 'Solution part1 ' . $sol1 . PHP_EOL;
    $sol2 = part2($parsed);
    echo 'Solution part2 ' . $sol2 . PHP_EOL;
}

solve();