<?php

function parse_file(): array
{
    $contents = preg_split("/\n/", rtrim(file_get_contents('./input.in')));
    return array_map(function ($l) {
        return preg_split("//", $l, -1, PREG_SPLIT_NO_EMPTY);
    }, $contents);
}

function part1(array $lines, int $rightSlope = 3, int $downSlope = 1): int
{
    $maxRight = count($lines[0]);
    $treeCount = 0;
    $right = 0;
    $down = 0;

    while($down < count($lines)) {
        if($lines[$down][$right] == "#") {
            $treeCount++;
        }
        $right += $rightSlope;
        if($right >= $maxRight) {
            $right -= $maxRight;
        }
        $down += $downSlope;
    }

    return $treeCount;
}

function part2(array $lines): int
{
    return part1($lines, rightSlope:1) *  part1($lines) *  part1($lines, rightSlope:5) *  part1($lines, rightSlope:7) * part1($lines, rightSlope:1, downSlope:2);
}

function solve(): void
{
    $parsed = parse_file();
    $sol1 = part1($parsed);
    echo 'Solution part1 ' . $sol1 . PHP_EOL;
    $sol2 = part2($parsed);
    echo 'Solution part2 ' . $sol2 . PHP_EOL;
}

solve();