# Advent of code

Advent of Code https://adventofcode.com/

## 2021 (python)

- [Day 1: Sonar Sweep](2021/day-01/)
- [Day 2: Dive](2021/day-02/)
- [Day 3: Binary Diagnostic](2021/day-O3/)
- [Day 4: Giant Squid](2021/day-04/)
- [Day 5: Hydrothermal Venture](2021/day-05/)
- [Day 6: Lanternfish](2021/day-06/)
- [Day 7: The Treachery of Whales](2021/day-07/)
- [Day 8: Seven Segment Search](2021/day-08/)
- [Day 9: Smoke Basin](2021/day-09/)
- [Day 10: Syntax Scoring](2021/day-10/)
- [Day 11: TBD](2021/day-11/)
- [Day 12: TBD](2021/day-12/)
- [Day 13: TBD](2021/day-13/)
- [Day 14: TBD](2021/day-14/)
- [Day 15: TBD](2021/day-15/)
- [Day 16: TBD](2021/day-16/)
- [Day 17: TBD](2021/day-17/)
- [Day 18: TBD](2021/day-18/)
- [Day 19: TBD](2021/day-19/)
- [Day 20: TBD](2021/day-20/)
- [Day 21: TBD](2021/day-21/)
- [Day 22: TBD](2021/day-22/)
- [Day 23: TBD](2021/day-23/)
- [Day 24: TBD](2021/day-24/)
- [Day 25: TBD](2021/day-25/)

## 2020 (php)

- [Day 1: Report Repair](2020/day-01/)
- [Day 2: Password Philosophy](2020/day-02/)
- [Day 3: Toboggan Trajectory](2020/day-03/)

